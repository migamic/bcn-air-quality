import simulation as sy
import networkx as nx
import matplotlib.pyplot as plt

G = sy.main()
colors = nx.get_edge_attributes(G, 'traf')
n_colors = nx.get_node_attributes(G, 'distr')
# pos = nx.spectral_layout(G)
# pos = nx.spring_layout(G, pos=pos)
posx = nx.get_node_attributes(G,'x')
posy = nx.get_node_attributes(G,'y')
pos = {i:(posx[i],posy[i]) for i in posx.keys()}
nx.draw(G,node_size=0, node_color=list(n_colors.values()), pos=pos, edge_color=list(colors.values()), edge_cmap=plt.cm.plasma_r, arrowstyle='-')
plt.show()

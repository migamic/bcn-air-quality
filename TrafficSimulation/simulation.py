import networkx as nx
import osmnx as ox
import random
import csv
import json
from pyproj import Transformer
import numpy as np

ox.config(use_cache=True)


# Reads graph data
def read_graph():
    # return nx.grid_graph([10,10])
    # return nx.random_geometric_graph(50, 0.3)
    g = ox.graph_from_point((41.392,2.15), dist=5000, network_type="drive")
    g = ox.add_edge_speeds(g)
    g = ox.add_edge_travel_times(g)
    return g


# Returns a dict of hyperparameters
def read_params():
    par = {
            'num_sims' : 1000,        # Number of person simulations
            'rem_road' : ['Avinguda Diagonal','Avinguda Diagonal (lateral muntanya)', 'Avinguda Diagonal (lateral mar)'],
            'speed_mod' : {'Carrer d\'Aragó' : 2, 'Ronda del Mig' : 0.001},
            'random_init' : False
            }
#    par['rem_road'] = []; par['speed_mod'] = {}

    return par


# Returns a dict with the nodes of the graph for each neighbourhood
def read_geodata(g,par):
    distr = {}
    distr_coords = [(0,0)]*74 # Initialize empty for each distr

    # Load districts and their data

    # Load distr num, name and population
    with open('data/2019_densitat.csv', newline='') as fdens:
        densreader = csv.reader(fdens, delimiter=',', quotechar='"')
        next(densreader) # Skip the header
        for row in densreader:
            distr[row[3]] = {'name' : row[4], 'pop' : row[5], 'nodes' : []}

    # Load centroid coordinate
    # transformer = Transformer.from_crs("epsg:25831", "epsg:4326")
    transformer = Transformer.from_crs("EPSG:25831", "EPSG:4326")
    with open('data/0301100100_UNITATS_ADM_PUNTS.json',newline='') as fcoord:
        data = json.load(fcoord)
        for f in data['features']:
            coor_x = f['geometry']['coordinates'][1]
            coor_y = f['geometry']['coordinates'][0]
            new_y, new_x = transformer.transform(coor_y, coor_x)
            if f['properties']['TIPUS_UA'] == 'BARRI':
                distr[str(int(f['properties']['BARRI']))]['x'] = new_x
                distr[str(int(f['properties']['BARRI']))]['y'] = new_y
                distr_coords[int(f['properties']['BARRI'])] = (new_x,new_y)

    distr_coords.pop(0) # Remove empty element (no ID for 0)
    # Assign nodes to each district
    coords_x = nx.get_node_attributes(g,'x')
    coords_y = nx.get_node_attributes(g,'y')

    assignations = {}

    for n in g.nodes:
        node_x = float(coords_x[n])
        node_y = float(coords_y[n])
        node_coords = np.array((node_x, node_y))

        min_dist = 1e5
        min_idx = 0
        for i,d in enumerate(distr_coords):
            d_coords = np.array(d)
            dist = np.linalg.norm(node_coords-d_coords)
            if dist < min_dist:
                min_idx = i
                min_dist = dist


        distr[str(min_idx+1)]['nodes'].append(n)
        assignations[n] = min_idx

    par['distr'] = distr
    par['assignations'] = assignations


    nx.set_node_attributes(g, assignations, 'distr')

# Modifies the graph with the user parameters
def sim_modify_graph(g,par):

    # Remove roads
    edgs = list(g.edges.keys()).copy()
    for e in edgs:
        data = g.get_edge_data(e[0],e[1])[0]
        if 'name' in data:
            # Delete the edge if its name is in par
            if data['name'] in par['rem_road']:
                g.remove_edge(e[0],e[1])

    # Change speeds
    for e in g.edges:
        data = g.get_edge_data(e[0],e[1])[0]
        if 'name' in data:
            # Modify travel_time
            if data['name'] in list(par['speed_mod'].keys()):
                new_time = g.get_edge_data(e[0],e[1])[0]['travel_time'] * 1/par['speed_mod'][data['name']]
                nx.set_edge_attributes(g, {(e[0],e[1],0): {0: {'travel_time' : new_time}}})


# Computes a np array with the probabilty of arrival and end at each neighbourhood
def sim_set_distr_prob(g,par):
    pops = []
    for d in par['distr'].keys():
        pops.append(par['distr'][d]['pop'])

    init_probs = np.array(pops, dtype=int)
    init_probs = init_probs/init_probs.sum()
    end_probs = 1-init_probs
    end_probs = end_probs/end_probs.sum()

    par['init_probs'] = init_probs
    par['end_probs'] = end_probs


# Returns the start and end of the route
def sim_start_end(g, par):
    num_nodes = len(g.nodes)

    if par['random_init']:
        start = list(g.nodes)[random.randint(0,num_nodes-1)]
        end = list(g.nodes)[random.randint(0,num_nodes-1)]
        return start, end

    # Sample with district distribution
    chosen_init = np.random.choice(range(len(par['distr'])), p=par['init_probs'])
    chosen_end = np.random.choice(range(len(par['distr'])), p=par['end_probs'])
    if len(par['distr'][str(chosen_init+1)]['nodes']) == 0:
        start = list(g.nodes)[random.randint(0,num_nodes-1)]
    else:
        start = par['distr'][str(chosen_init+1)]['nodes'][random.randint(0,len(par['distr'][str(chosen_init+1)]['nodes'])-1)]
    if len(par['distr'][str(chosen_end+1)]['nodes']) == 0:
        end = list(g.nodes)[random.randint(0,num_nodes-1)]
    else:
        end = par['distr'][str(chosen_end+1)]['nodes'][random.randint(0,len(par['distr'][str(chosen_end+1)]['nodes'])-1)]

    return start, end


# Returns a route chosen by the user
def sim_compute_route(g, par, start, end):
    try:
        return nx.shortest_path(g, start, end, weight='travel_time')
    except:
        # If no route available, return empty path
        return []


# Add one to num_traf for every edge from the route
def sim_update_traf(g, par, route, num_traf):
    for n in range(len(route)-1):
        if (route[n], route[n+1], 0) in num_traf:
            num_traf[(route[n],route[n+1], 0)] += 1
        elif (route[n+1], route[n], 0) in num_traf:
            num_traf[(route[n+1],route[n], 0)] += 1
        else:
            pass
            # raise Exception('Invalid route edge')


# Given a graph and hyperparameters, returns the congestion of each street
def run_simulation(g, par):
    num_traf = {key:0 for key in g.edges}
    nopath=0

    sim_set_distr_prob(g,par)

    sim_modify_graph(g,par)
    for s in range(par['num_sims']):
        start, end = sim_start_end(g,par)
        route = sim_compute_route(g,par,start,end)
        sim_update_traf(g,par,route,num_traf)
        if route == []: nopath += 1

    nx.set_edge_attributes(g, num_traf, name='traf')
    print('Proportion of incomplete paths:', nopath/par['num_sims'])
    print(max(list(num_traf.values())))



def parse_result(g):
    return g.edges.data()


def main():
    g = read_graph()
    par = read_params()
    distr = read_geodata(g,par)
    run_simulation(g, par)
    return g


if __name__ == "__main__":
    main()

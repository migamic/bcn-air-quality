
// MAP PROJECTION (NO TOCAR PORFA)


var div_id_map = '#map';
var div_id_line = '#series';
var div_id_cal = '#calendar-viz';

var col =  d3.scaleSequential(d3.interpolateReds).domain([0, 5])

function drawMap(data, data_day, col, hour=12, bcnout=null) {

    var w = 700;
    var h = 700;
    var projection = d3.geoMercator().translate([w/2, h/2]).scale(195000).center([2.15,41.392]);
    let geoGenerator = d3.geoPath()
                         .projection(projection);
    document.querySelector(div_id_map).innerHTML = "";

    var svgMap = d3.select(div_id_map).append("svg").attr("preserveAspectRatio", "xMinYMin meet")
            .attr("width", w).attr("height", h)
            .attr("viewBox", "0 0 " + w + " " + h)
            .classed("svg-content", true);
    var data_day_hour1 = data_day.filter( d => parseInt(d['Dia'].split(" ")[1].split(":")[0]) == hour );
    console.log(data_day)
    console.log(data_day_hour1)
    var tram_data = Object.assign(...data_day_hour1.map(d => ({ [d['idTram']]: parseFloat(d['estatActual']) })));
    console.log(tram_data)

    if (bcnout != null) {
        console.log(bcnout)
        svgMap.selectAll("path")
              .data(bcnout.features)
              .join('path')
              .attr("fill", "#DDDDDD")
              .attr("class", "outline")
              .attr('d', geoGenerator);
    }

    svgMap.selectAll("line")
        .data(data)
        .enter()
        .append("line")
        .attr("x1", (d) => projection([d.x_lat, d.x_lon])[0])
        .attr("y1", (d) => projection([d.x_lat, d.x_lon])[1])
        .attr("x2", (d) => projection([d.y_lat, d.x_lon])[0])
        .attr("y2", (d) => projection([d.y_lat, d.y_lon])[1])
        .style("stroke", d => {
            //console.log(d['idTram'])
            return col(tram_data[parseInt(d['Tram'])])

    })
}

function drawLineChart(conthour, lim, dia, mes) {
    console.log("......... ", conthour)
    console.log("......... ", dia, mes)
    var dataxhour = conthour.filter(d => 
                            (parseInt(d['DIA']) == dia) && (parseInt(d['MES']) == mes))
    
                            console.log("----", dataxhour)
    let last_point = dataxhour[lim];
    console.log("-----", last_point)
    var w = 400;
    var h = 400;

    document.querySelector(div_id_line).innerHTML = "";
    var svgChart = d3.select(div_id_line).append("svg").attr("preserveAspectRatio", "xMinYMin meet")
            .attr("width", w).attr("height", h)
            .attr("viewBox", "0 0 " + w + " " + h)
            .classed("svg-content", true);

    var x = d3.scaleLinear()
        .domain([0, 23])
        .range([ 30, w-10 ]);
    svgChart.append("g")
        .attr("transform", "translate(0," + (h-40) + ")")
        .call(d3.axisBottom(x));

    var y = d3.scaleLinear()
        .domain([0, 100])//d3.max(dataxhour, d => +d['variable']+10)])
        .range([ h-40, 10 ]);
    svgChart.append("g")
        .attr("transform", "translate(" + 30 +  ",0" + ")")
        .call(d3.axisLeft(y));

      // Add the line
    svgChart.append("path")
      .datum(dataxhour)
      .attr("fill", "none")
      .attr("stroke", "steelblue")
      .attr("stroke-width", 2)
      .attr("d", d3.line()
        .x(d => x(d['H']))
        .y(d => y(d['variable']))
        )
    
    svgChart.append("circle")
        .attr("fill", "steelblue")
        .attr("cx", x(last_point['H']))
        .attr("cy", y(last_point['variable']))
        .attr("r", 5)
}

function drawCalendar(dailyData) {
    var w = 600;
    var h = 200;

    document.querySelector(div_id_cal).innerHTML = "";
    var svgCal = d3.select(div_id_cal).append("svg").attr("preserveAspectRatio", "xMinYMin meet")
            .attr("width", w).attr("height", h)
            .attr("viewBox", "0 0 " + w + " " + h)
            .classed("svg-content", true);

    var x = d3.scaleLinear()
        .domain([0, 53])
        .range([ 40, w ]);
    /*svgCal.append("g")
        .attr("transform", "translate(0," + (h-40) + ")")
        .call(d3.axisBottom(x));*/

    const DIESSET = ["Dl", "Dm", "Dx", "Dj", "Dv", "Ds", "Dg"]
    var y = d3.scaleBand()
        .domain(DIESSET)
        .range([ 0, h ]);
    svgCal.append("g")
        .attr("transform", "translate(" + 40 +  ",0" + ")")
        .call(d3.axisLeft(y));

    // add the squares
    function handleCalClick(dia) {
        let lab = document.querySelector('#label-dia');
        let date = new Date('2021-01-01');
        date.setDate(date.getDate() + parseInt(dia['tot_dia']));
        DIA = parseInt(date.getDate()); MES = parseInt(date.getMonth())+1;
        lab.innerHTML = `${date.getDate()} - ${date.getMonth()+1} - 2021`;
        filterData(parseInt(date.getDate())+1,parseInt(date.getMonth())+1);
        console.log(DATA_DAY);
        let selectVal = parseInt(document.querySelector('#range').value);
        console.log(selectVal);
        drawMap(DATA, DATA_DAY, col, 0, BCNOUT);
        drawLineChart(CONTHORA, parseInt(evt.target.value), DIA, MES);
    }
    var col = d3.scaleSequential(d3.interpolateReds)
                .domain([-1, 100])
    var thedata = [...Array(365).keys()].map((idx) => ({'tot_dia': idx, 'day': (idx+4) % 7, 'col': Math.floor((idx+4)/7) }));
    svgCal.selectAll()
    .data(thedata)
    .enter()
    .append("rect")
        .attr("class", "hoverable")
        .attr("x", d => x(d['col']) )
        .attr("y", d => y(DIESSET[d['day']]) )
        .attr("width", (w-40)/55 )
        .attr("height", (h-50)/7 )
        .style("fill", d => {
            if (parseInt(d['tot_dia']) > 302)
                return 'lightgray'
            else {
                let date = new Date('2021-01-01');
                date.setDate(date.getDate() + parseInt(d['tot_dia']));
                date.setHours(0);
                //console.log(new Date(dailyData[500]['Dia']).getTime() == date.getTime())
                let meanCont = dailyData.filter(d => ((new Date(d['Dia'])).getTime() == date.getTime()))[0]["Mitjana NO2"];
                return col(meanCont);
            }
        })
        //.style("stroke-width", 4)
        //.style("stroke", "none")
        .style("opacity", 1)
    .on("click", (_, d) => handleCalClick(d))
    //.on("mousemove", mousemove)
    //.on("mouseleave", mouseleave)
}

var ALL_DATA;
var DATA_DAY;
var DATA;
var DATAXHOUR;
var BCNOUT;
var CONTHORA;
var DIA = 1; var MES = 1;


async function filterData(day, month) {
    DATA_DAY = ALL_DATA.filter( d => {
        var [y, m, d] = d['Dia'].split(" ")[0].split("-");
        m = parseInt(m); d = parseInt(d);
        return  m == month && d == day;
    })
}

//d3.csv("tday1.csv").then( data_day => {
d3.csv('contaminacio_hora.csv').then( conthora => {
    d3.json('bcnoutline.geojson').then( bcnout => {
        d3.csv("transit_hores.csv").then( data_all => {
            d3.csv("trams.csv").then( data => {
                d3.csv("contxhour.csv").then( dataxhour => {
                    d3.csv("chart.csv").then( dailyData => {
                        ALL_DATA = data_all;
                        DATAXHOUR = dataxhour;
                        DATA = data;
                        BCNOUT = bcnout;
                        CONTHORA = conthora;
                        //DATA_DAY = data_day;
                        filterData(1, 1);
                        drawMap(data, DATA_DAY, col, 0, bcnout);
                        drawLineChart(conthora, 0, DIA, MES);
                        drawCalendar(dailyData);
                    })
                })
            });
        });
    });
} )


document.querySelector("#range").addEventListener('change', evt => {
    console.log(evt.target.value)
    drawMap(DATA, DATA_DAY, col, evt.target.value, BCNOUT);
    drawLineChart(CONTHORA, parseInt(evt.target.value), DIA, MES);
})



/*
document.querySelector("input[type='number']").addEventListener('change', () => {
    filterData();
});*/
 